package configuration;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import org.apache.commons.lang3.StringUtils;

import java.io.IOException;
import java.io.Reader;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

public class ConfigurationManager {
    private final static Logger LOGGER = Logger.getLogger(ConfigurationManager.class.getName());

    private Map<String, ResponseConfiguration> callConfiguration;
    private final String configurationFilePath;

    public String getConfigurationFilePath() {
        return configurationFilePath;
    }

    public ConfigurationManager(String configurationFilePath) {
        this.configurationFilePath = configurationFilePath;
        this.loadConfigurationFromFile();
    }

    private void loadConfigurationFromFile() {
        this.clearConfiguration();

        try {

            if (StringUtils.isEmpty(this.configurationFilePath)) {
                LOGGER.log(Level.SEVERE, "Configuration file path is empty, default success configuration will be used");
                this.generateDefaultConfiguration();
                return;
            }

            if (!Files.exists(Paths.get(this.configurationFilePath))) {
                LOGGER.log(Level.SEVERE, "Configuration file path does not exists, default success configuration will be used");
                this.generateDefaultConfiguration();
                return;
            }

            Reader reader = Files.newBufferedReader(Paths.get(this.configurationFilePath));
            List<ResponseConfiguration> fileContent = new Gson().fromJson(reader, new TypeToken<List<ResponseConfiguration>>() {
            }.getType());

            for (ResponseConfiguration currentConfig : fileContent) {
                this.callConfiguration.put(currentConfig.getResponseProcessorClass(), currentConfig);
            }
            reader.close();
        } catch (IOException e) {
            LOGGER.log(Level.SEVERE, "Error while reading mock configuration file, default success configuration will be used", e);
            this.generateDefaultConfiguration();
        }
    }

    private void generateDefaultConfiguration() {
        ResponseConfiguration checkBundleSubscriptionConfig = new ResponseConfiguration();
        checkBundleSubscriptionConfig.setResponseProcessorClass("responseProcessors.CheckBundleSubscriptionResponseProcessor");
        checkBundleSubscriptionConfig.setResponseCode("00");
        checkBundleSubscriptionConfig.setResponseMessage("Successful");
        checkBundleSubscriptionConfig.setResponseDelay(0);
        callConfiguration.put("responseProcessors.CheckBundleSubscriptionResponseProcessor", checkBundleSubscriptionConfig);

        ResponseConfiguration bundleTopupConfig = new ResponseConfiguration();
        bundleTopupConfig.setResponseProcessorClass("responseProcessors.BundleTopupResponseProcessor");
        bundleTopupConfig.setResponseCode("00");
        bundleTopupConfig.setResponseMessage("Successful");
        bundleTopupConfig.setResponseDelay(0);
        callConfiguration.put("responseProcessors.BundleTopupResponseProcessor", bundleTopupConfig);

        ResponseConfiguration bundleQueryConfig = new ResponseConfiguration();
        bundleQueryConfig.setResponseProcessorClass("responseProcessors.BundleQueryResponseProcessor");
        bundleQueryConfig.setResponseCode("00");
        bundleQueryConfig.setResponseMessage("Successful");
        bundleQueryConfig.setResponseDelay(0);
        callConfiguration.put("responseProcessors.BundleQueryResponseProcessor", bundleQueryConfig);

        ResponseConfiguration bundleTopupVoidConfig = new ResponseConfiguration();
        bundleTopupVoidConfig.setResponseProcessorClass("responseProcessors.BundleTopupVoidResponseProcessor");
        bundleTopupVoidConfig.setResponseCode("00");
        bundleTopupVoidConfig.setResponseMessage("Successful");
        bundleTopupVoidConfig.setResponseDelay(0);
        callConfiguration.put("responseProcessors.BundleTopupVoidResponseProcessor", bundleQueryConfig);
    }

    private void clearConfiguration() {
        if (callConfiguration != null) {
            this.callConfiguration.clear();
        } else {
            this.callConfiguration = new HashMap<>();
        }
    }

    public ResponseConfiguration getResponseProcessorConfiguration(String className) {
        ResponseConfiguration retValue = null;
        if ((!StringUtils.isEmpty(className)) && (this.callConfiguration.containsKey(className))) {
            retValue = this.callConfiguration.get(className);
        }
        return retValue;
    }
}
