package configuration;

public class ResponseConfiguration {
    private String responseProcessorClass;
    private String responseCode;
    private String responseMessage;
    private int responseDelay;

    public String getResponseProcessorClass() {
        return responseProcessorClass;
    }

    public void setResponseProcessorClass(String responseProcessorClass) {
        this.responseProcessorClass = responseProcessorClass;
    }

    public String getResponseCode() {
        return responseCode;
    }

    public void setResponseCode(String responseCode) {
        this.responseCode = responseCode;
    }

    public String getResponseMessage() {
        return responseMessage;
    }

    public void setResponseMessage(String responseMessage) {
        this.responseMessage = responseMessage;
    }

    public int getResponseDelay() {
        return responseDelay;
    }

    public void setResponseDelay(int responseDelay) {
        this.responseDelay = responseDelay;
    }

    public ResponseConfiguration() {
        this.responseProcessorClass = null;
        this.responseCode = "00";
        this.responseMessage = "Successful";
        this.responseDelay = 0;
    }

    public ResponseConfiguration(String responseProcessorClass, String responseCode, String responseMessage,
                                 int responseDelay) {
        this.responseProcessorClass = responseProcessorClass;
        this.responseCode = responseCode;
        this.responseMessage = responseMessage;
        this.responseDelay = responseDelay;
    }

    @Override
    public String toString() {
        return "ResponseConfiguration{" +
                "responseProcessorClass='" + responseProcessorClass + '\'' +
                ", responseCode='" + responseCode + '\'' +
                ", responseMessage='" + responseMessage + '\'' +
                ", responseDelay=" + responseDelay +
                '}';
    }
}
