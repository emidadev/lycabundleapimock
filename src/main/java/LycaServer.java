import configuration.ConfigurationManager;

import java.io.IOException;
import java.net.ServerSocket;
import java.util.logging.Logger;

public class LycaServer {

    private final static Logger LOGGER = Logger.getLogger(LycaServer.class.getName());

    private ServerSocket serverSocket;

    public void start(int port, ConfigurationManager configurationManager) throws IOException {
        serverSocket = new ServerSocket(port);
        LOGGER.info(String.format("Lyca Mock Server listening on port : %d", port));

        while (true) {
            (new ClientDispatcher(this.serverSocket.accept(), configurationManager)).start();
        }
    }

    public void stop() throws IOException {
        serverSocket.close();
    }
}
