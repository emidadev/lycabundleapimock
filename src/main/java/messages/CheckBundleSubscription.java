package messages;

import org.w3c.dom.Document;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import java.io.ByteArrayInputStream;

public class CheckBundleSubscription extends CMessageRequest implements IMessage
{
	// constants
	public static final String MESSAGE_FORMAT = "<CHECKREQUEST><MESSAGETYPE>%s</MESSAGETYPE><PRODUCTID>%s</PRODUCTID><BUNDLEID>%s</BUNDLEID><BUNDLENAME>%s</BUNDLENAME><MSISDN>%s</MSISDN><AMOUNT>%s</AMOUNT><TXID>%s</TXID><TERMINALID>%s</TERMINALID><CURRENCY>%s</CURRENCY><NO_OF_MONTHS>%s</NO_OF_MONTHS></CHECKREQUEST>";
	
	public static final String MESSAGE_TYPE = "CHECK BUNDLE SUBSCRIPTION"; 

	
	// attributes
	String productId;
	String bundleId;
	String bundleName;
	String msisdn;
	String amount;
	String transactionId;
	String terminalId;
	String currency;
	String numberOfMonths;

	
	// properties
	
	public String getProductId()
	{
		return productId;
	}

	public void setProductId(String productId)
	{
		this.productId = productId;
	}

	public String getBundleId()
	{
		return bundleId;
	}

	public void setBundleId(String bundleId)
	{
		this.bundleId = bundleId;
	}

	public String getBundleName()
	{
		return bundleName;
	}

	public void setBundleName(String bundleName)
	{
		this.bundleName = bundleName;
	}

	public String getMsisdn()
	{
		return msisdn;
	}

	public void setMsisdn(String msisdn)
	{
		this.msisdn = msisdn;
	}

	public String getAmount()
	{
		return amount;
	}

	public void setAmount(String amount)
	{
		this.amount = amount;
	}

	public String getTransactionId()
	{
		return transactionId;
	}

	public void setTransactionId(String transactionId)
	{
		this.transactionId = transactionId;
	}
	

	public String getTerminalId()
	{
		return terminalId;
	}

	public void setTerminalId(String terminalId)
	{
		this.terminalId = terminalId;
	}

	public String getCurrency()
	{
		return currency;
	}

	public void setCurrency(String currency)
	{
		this.currency = currency;
	}

	public String getNumberOfMonths() {
		return numberOfMonths;
	}

	public void setNumberOfMonths(String numberOfMonths) {
		this.numberOfMonths = numberOfMonths;
	}

	@Override
	public void setContent(String xmlContent) throws Exception
	{
		try
		{
			if((xmlContent == null) || ((xmlContent != null) && (xmlContent.trim().equals(""))))
			{
				throw new IllegalArgumentException("xmlContent empty");
			}
			DocumentBuilderFactory docBuilderFactory = DocumentBuilderFactory.newInstance();
			DocumentBuilder docBuilder = docBuilderFactory.newDocumentBuilder();
			Document doc = docBuilder.parse(new ByteArrayInputStream(xmlContent.replaceAll(">\\s*<", "><").trim().getBytes("US-ASCII")));
			doc.getDocumentElement().normalize();
			String messageType = doc.getDocumentElement().getNodeName();
			if (messageType.equalsIgnoreCase("CHECKREQUEST")) // Initial message tag, not the message type
			{
				// this.messageType = MESSAGE_TYPE; already set in the constructor, left here as information
				// Search for product id
				this.productId = MessageTools.getStringValue("PRODUCTID", doc);
				// Search for bundle id
				this.bundleId = MessageTools.getStringValue("BUNDLEID", doc);
				// Search for bundle name
				this.bundleName = MessageTools.getStringValue("BUNDLENAME", doc);
				// Search for msisdn
				this.msisdn = MessageTools.getStringValue("MSISDN", doc);
				// Search for amount
				this.amount = MessageTools.getStringValue("AMOUNT", doc);
				// Search for transaction id
				this.transactionId = MessageTools.getStringValue("TXID", doc);
				// Search for terminal id
				this.terminalId = MessageTools.getStringValue("TERMINALID", doc);
				// Search for currency
				this.currency = MessageTools.getStringValue("CURRENCY", doc);
				// Number of months
				this.numberOfMonths = MessageTools.getStringValue("NO_OF_MONTHS", doc);
			}
			else
			{
				throw new Exception("XML content does not corresponds to a checkrequest message");
			}
		} 
		catch (Exception localException)
		{
			throw localException;
		}
	}

	@Override
	public String getContent()
	{
		return String.format(MESSAGE_FORMAT, this.messageType, this.productId, this.bundleId, this.bundleName, this.msisdn, this.amount, this.transactionId, this.terminalId, this.currency, this.numberOfMonths);
	}

	@Override
	public int getLength()
	{
		return this.getContent().length();
	}


	// methods
	public CheckBundleSubscription()
	{
		super();
		this.messageType = MESSAGE_TYPE;
		this.productId = null;
		this.bundleId = null;
		this.bundleName = null;
		this.msisdn = null;
		this.amount = null;
		this.transactionId = null;
		this.transactionId = null;
		this.currency = null;
		this.numberOfMonths = null;
	}

	
	
	public CheckBundleSubscription(String productId, String bundleId, String bundleName, String msisdn,
								   String amount, String transactionId, String terminalId, String currency, String numberOfMonths)
	{
		this();
		this.productId = productId;
		this.bundleId = bundleId;
		this.bundleName = bundleName;
		this.msisdn = msisdn;
		this.amount = amount;
		this.transactionId = transactionId;
		this.terminalId = terminalId;
		this.currency = currency;
		this.numberOfMonths = numberOfMonths;
	}

}
