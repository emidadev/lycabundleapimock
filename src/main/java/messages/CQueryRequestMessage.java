/**
 *
 */
package messages;

import org.w3c.dom.Document;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import java.io.ByteArrayInputStream;

/**
 * @author fernandob
 *
 */
public class CQueryRequestMessage extends CMessageRequest implements IMessage {
    // constants
    public static final String MESSAGE_FORMAT = "<QUERYREQUEST><MESSAGETYPE>%s</MESSAGETYPE><TXID>%s</TXID><TXREF>%s</TXREF></QUERYREQUEST>";

    public static final String MESSAGE_TYPE = "QUERY";

    // attributes
    String transactionId;
    String referenceTransactionId;


    // properties

    public String getTransactionId() {
        return transactionId;
    }

    public void setTransactionId(String transactionId) {
        this.transactionId = transactionId;
    }

    public String getReferenceTransactionId() {
        return referenceTransactionId;
    }

    public void setReferenceTransactionId(String referenceTransactionId) {
        this.referenceTransactionId = referenceTransactionId;
    }

    // methods


    /* (non-Javadoc)
     * @see com.debisys.h2h.providers.lycaMobile.V1010.Messages.IMessage#getContent()
     */
    @Override
    public String getContent() {
        return String.format(MESSAGE_FORMAT, this.messageType, this.transactionId, this.referenceTransactionId);
    }

    /* (non-Javadoc)
     * @see com.debisys.h2h.providers.lycaMobile.V1010.Messages.IMessage#getLength()
     */
    @Override
    public int getLength() {
        return this.getContent().length();
    }

    /* (non-Javadoc)
     * @see com.debisys.h2h.providers.lycaMobile.V1010.Messages.IMessage#setContent(java.lang.String)
     */
    @Override
    public void setContent(String xmlContent) throws Exception {
        try {
            if ((xmlContent == null) || ((xmlContent != null) && (xmlContent.trim().equals("")))) {
                throw new IllegalArgumentException("xmlContent empty");
            }

            DocumentBuilderFactory docBuilderFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder docBuilder = docBuilderFactory.newDocumentBuilder();
            Document doc = docBuilder.parse(new ByteArrayInputStream(xmlContent.replaceAll(">\\s*<", "><").trim().getBytes("US-ASCII")));
            doc.getDocumentElement().normalize();
            String messageType = doc.getDocumentElement().getNodeName();
            if (messageType.equalsIgnoreCase("QUERYREQUEST")) // Initial message tag, not the message type
            {
                // this.messageType = MESSAGE_TYPE; already set in the constructor, left here as information

                // 1. Search for reference transaction id
                this.referenceTransactionId = MessageTools.getStringValue("TXREF", doc);
                // 2. Search for transaction id
                this.transactionId = MessageTools.getStringValue("TXID", doc);
            } else {
                throw new Exception("XML content does not corresponds to a checkrequest message");
            }
        } catch (Exception localException) {
            throw localException;
        }
    }


    public CQueryRequestMessage() {
        super();
        this.messageType = MESSAGE_TYPE;
        this.transactionId = null;
        this.referenceTransactionId = null;
    }


    public CQueryRequestMessage(String transactionId, String referenceTransactionId) {
        this();
        this.transactionId = transactionId;
        this.referenceTransactionId = referenceTransactionId;
    }


}
