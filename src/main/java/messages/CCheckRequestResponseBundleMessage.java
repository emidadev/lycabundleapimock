package messages;

import org.w3c.dom.Document;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import java.io.ByteArrayInputStream;

public class CCheckRequestResponseBundleMessage implements IMessage {
    // constants
    public static final String MESSAGE_FORMAT = "<CHECKRESPONSE><RESULT>%s</RESULT><RESULTTEXT>%s</RESULTTEXT><TXID>%s</TXID><TERMINALID>%s</TERMINALID><PRODUCTID>%s</PRODUCTID><BUNDLEID>%s</BUNDLEID><BUNDLENAME>%s</BUNDLENAME><MSISDN>%s</MSISDN><TOTAL_INSTALLMENTS_COST>%s</TOTAL_INSTALLMENTS_COST></CHECKRESPONSE>";


    // attributes
    String resultCode;
    String resultText;
    String transactionId;
    String terminalId;
    String productId;
    String bundleId;
    String bundleName;
    String msisdn;
    String totalInstallmentsCost;

    // properties
    public String getResultCode() {
        return resultCode;
    }

    public void setResultCode(String resultCode) {
        this.resultCode = resultCode;
    }

    public String getResultText() {
        return resultText;
    }

    public void setResultText(String resultText) {
        this.resultText = resultText;
    }

    public String getTransactionId() {
        return transactionId;
    }

    public void setTransactionId(String transactionId) {
        this.transactionId = transactionId;
    }

    public String getTerminalId() {
        return terminalId;
    }

    public void setTerminalId(String terminalId) {
        this.terminalId = terminalId;
    }

    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }

    public String getBundleId() {
        return bundleId;
    }

    public void setBundleId(String bundleId) {
        this.bundleId = bundleId;
    }

    public String getBundleName() {
        return bundleName;
    }

    public void setBundleName(String bundleName) {
        this.bundleName = bundleName;
    }

    public String getMsisdn() {
        return msisdn;
    }

    public void setMsisdn(String msisdn) {
        this.msisdn = msisdn;
    }

    public String getTotalInstallmentsCost() {
        return totalInstallmentsCost;
    }

    public void setTotalInstallmentsCost(String totalInstallmentsCost) {
        this.totalInstallmentsCost = totalInstallmentsCost;
    }

    @Override
    public String getContent() {
        return String.format(MESSAGE_FORMAT, this.resultCode, this.resultText, this.transactionId, this.terminalId,
                this.productId, this.bundleId, this.bundleName, this.msisdn, this.totalInstallmentsCost);
    }

    @Override
    public int getLength() {
        return this.getContent().length();
    }

    @Override
    public void setContent(String xmlContent) throws Exception {
        try {
            if ((xmlContent == null) || ((xmlContent != null) && (xmlContent.trim().equals("")))) {
                throw new IllegalArgumentException("xmlContent empty");
            }

            DocumentBuilderFactory docBuilderFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder docBuilder = docBuilderFactory.newDocumentBuilder();
            Document doc = docBuilder.parse(new ByteArrayInputStream(xmlContent.replaceAll(">\\s*<", "><").trim().getBytes("US-ASCII")));
            doc.getDocumentElement().normalize();
            String messageType = doc.getDocumentElement().getNodeName();
            if (messageType.equalsIgnoreCase("CHECKRESPONSE")) {
                // this.messageType = MESSAGE_TYPE; already set in the constructor, left here as information
                // Search for result code
                this.resultCode = MessageTools.getStringValue("RESULT", doc);
                // Search for result text
                this.resultText = MessageTools.getStringValue("RESULTTEXT", doc);
                // Search for transaction id
                this.transactionId = MessageTools.getStringValue("TXID", doc);
                // Search for terminal id
                this.terminalId = MessageTools.getStringValue("TERMINALID", doc);
                // Search for product id
                this.productId = MessageTools.getStringValue("PRODUCTID", doc);
                // Search for bundle id
                this.bundleId = MessageTools.getStringValue("BUNDLEID", doc);
                // Search for bundle name
                this.bundleName = MessageTools.getStringValue("BUNDLENAME", doc);
                // Search for MSISDN - Account Number
                this.msisdn = MessageTools.getStringValue("MSISDN", doc);
                // Total installments costs
                this.totalInstallmentsCost = MessageTools.getStringValue("TOTAL_INSTALLMENTS_COST", doc);
            } else {
                throw new Exception("XML content does not corresponds to a checkrequest message");
            }
        } catch (Exception localException) {
            throw localException;
        }
    }


    public CCheckRequestResponseBundleMessage() {
        super();
        this.resultCode = null;
        this.resultText = null;
        this.transactionId = null;
        this.terminalId = null;
        this.productId = null;
        this.bundleId = null;
        this.bundleName = null;
        this.msisdn = null;
        this.totalInstallmentsCost = null;
    }

    public CCheckRequestResponseBundleMessage(String resultCode, String resultText, String transactionId,
                                              String terminalId, String productId, String bundleId, String bundleName, String msisdn,
                                              String totalInstallmentsCost) {
        this();
        this.resultCode = resultCode;
        this.resultText = resultText;
        this.transactionId = transactionId;
        this.terminalId = terminalId;
        this.productId = productId;
        this.bundleId = bundleId;
        this.bundleName = bundleName;
        this.msisdn = msisdn;
        this.totalInstallmentsCost = totalInstallmentsCost;
    }

}
