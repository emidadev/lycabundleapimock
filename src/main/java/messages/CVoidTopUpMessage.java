/**
 *
 */
package messages;

import org.w3c.dom.Document;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import java.io.ByteArrayInputStream;

/**
 * @author fernandob
 *
 */
public class CVoidTopUpMessage extends CMessageRequest implements IMessage {
    // constants
    public static final String MESSAGE_FORMAT = "<VOIDTOPUPREQUEST><MESSAGETYPE>%s</MESSAGETYPE><MSISDN>%s</MSISDN><TERMINALID>%s</TERMINALID><TXID>%s</TXID><CURRENCY>%s</CURRENCY><TXREF>%s</TXREF><AMOUNT>%s</AMOUNT></VOIDTOPUPREQUEST>";

    public static final String MESSAGE_TYPE = "VOID";

    // attributes

    String referenceTransactionId;
    String msisdn;
    String amount;
    String transactionId;
    String terminalId;
    String currency;

    // properties

    public String getReferenceTransactionId() {
        return referenceTransactionId;
    }

    public void setReferenceTransactionId(String referenceTransactionId) {
        this.referenceTransactionId = referenceTransactionId;
    }

    public String getMsisdn() {
        return msisdn;
    }

    public void setMsisdn(String msisdn) {
        this.msisdn = msisdn;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getTransactionId() {
        return transactionId;
    }

    public void setTransactionId(String transactionId) {
        this.transactionId = transactionId;
    }

    public String getTerminalId() {
        return terminalId;
    }

    public void setTerminalId(String terminalId) {
        this.terminalId = terminalId;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    // Methods


    /* (non-Javadoc)
     * @see com.debisys.h2h.providers.lycaMobile.V1010.Messages.IMessage#getContent()
     */
    @Override
    public String getContent() {
        return String.format(MESSAGE_FORMAT, this.messageType, this.msisdn, this.terminalId, this.transactionId, this.currency, this.referenceTransactionId, this.amount);
    }

    /* (non-Javadoc)
     * @see com.debisys.h2h.providers.lycaMobile.V1010.Messages.IMessage#getLength()
     */
    @Override
    public int getLength() {
        return this.getContent().length();
    }

    /* (non-Javadoc)
     * @see com.debisys.h2h.providers.lycaMobile.V1010.Messages.IMessage#setContent(java.lang.String)
     */
    @Override
    public void setContent(String xmlContent) throws Exception {
        try {
            if ((xmlContent == null) || ((xmlContent != null) && (xmlContent.trim().equals("")))) {
                throw new IllegalArgumentException("xmlContent empty");
            }

            DocumentBuilderFactory docBuilderFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder docBuilder = docBuilderFactory.newDocumentBuilder();
            Document doc = docBuilder.parse(new ByteArrayInputStream(xmlContent.replaceAll(">\\s*<", "><").trim().getBytes("US-ASCII")));
            doc.getDocumentElement().normalize();
            String messageType = doc.getDocumentElement().getNodeName();
            if (messageType.equalsIgnoreCase("VOIDTOPUPREQUEST")) // Initial message tag, not the message type
            {
                // this.messageType = MESSAGE_TYPE; already set in the constructor, left here as information
                // 1. Search for reference transaction id
                this.referenceTransactionId = MessageTools.getStringValue("TXREF", doc);
                // 2. Search for msisdn
                this.msisdn = MessageTools.getStringValue("MSISDN", doc);
                // 3. Search for amount
                this.amount = MessageTools.getStringValue("AMOUNT", doc);
                // 4. Search for transaction id
                this.transactionId = MessageTools.getStringValue("TXID", doc);
                // 5. Search for terminal id
                this.terminalId = MessageTools.getStringValue("TERMINALID", doc);
                // 6. Search for currency
                this.currency = MessageTools.getStringValue("CURRENCY", doc);
            } else {
                throw new Exception("XML content does not corresponds to a checkrequest message");
            }
        } catch (Exception localException) {
            throw localException;
        }
    }


    public CVoidTopUpMessage() {
        super();
        this.messageType = MESSAGE_TYPE;
        this.referenceTransactionId = null;
        this.msisdn = null;
        this.amount = null;
        this.transactionId = null;
        this.terminalId = null;
        this.currency = null;
    }


    public CVoidTopUpMessage(String referenceTransactionId, String msisdn, String amount, String transactionId,
                             String terminalId, String currency) {
        this();
        this.referenceTransactionId = referenceTransactionId;
        this.msisdn = msisdn;
        this.amount = amount;
        this.transactionId = transactionId;
        this.terminalId = terminalId;
        this.currency = currency;
    }


}
