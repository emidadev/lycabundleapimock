/**
 *
 */
package messages;

import org.w3c.dom.Document;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import java.io.ByteArrayInputStream;

/**
 * @author fernandob
 *
 */
public class CTopUpResponseMessage implements IMessage {
    // constants
    public static final String MESSAGE_FORMAT = "<TOPUPRESPONSE><RESULT>%s</RESULT><RESULTTEXT>%s</RESULTTEXT><TERMINALID>%s</TERMINALID><TXID>%s</TXID><PRODUCTID>%s</PRODUCTID></TOPUPRESPONSE>";

    // attributes
    String resultCode;
    String resultText;
    String terminalId;
    String transactionId;
    String productId;

    // properties
    public String getResultCode() {
        return resultCode;
    }

    public void setResultCode(String resultCode) {
        this.resultCode = resultCode;
    }

    public String getResultText() {
        return resultText;
    }

    public void setResultText(String resultText) {
        this.resultText = resultText;
    }

    public String getTerminalId() {
        return terminalId;
    }

    public void setTerminalId(String terminalId) {
        this.terminalId = terminalId;
    }

    public String getTransactionId() {
        return transactionId;
    }

    public void setTransactionId(String transactionId) {
        this.transactionId = transactionId;
    }

    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }

    // methods

    /* (non-Javadoc)
     * @see com.debisys.h2h.providers.lycaMobile.V1010.Messages.IMessage#getContent()
     */
    @Override
    public String getContent() {
        return String.format(MESSAGE_FORMAT, this.resultCode, this.resultText, this.terminalId, this.transactionId, this.productId);
    }

    /* (non-Javadoc)
     * @see com.debisys.h2h.providers.lycaMobile.V1010.Messages.IMessage#getLength()
     */
    @Override
    public int getLength() {
        return this.getContent().length();
    }

    /* (non-Javadoc)
     * @see com.debisys.h2h.providers.lycaMobile.V1010.Messages.IMessage#setContent(java.lang.String)
     */
    @Override
    public void setContent(String xmlContent) throws Exception {
        try {
            if ((xmlContent == null) || ((xmlContent != null) && (xmlContent.trim().equals("")))) {
                throw new IllegalArgumentException("xmlContent empty");
            }

            DocumentBuilderFactory docBuilderFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder docBuilder = docBuilderFactory.newDocumentBuilder();
            Document doc = docBuilder.parse(new ByteArrayInputStream(xmlContent.replaceAll(">\\s*<", "><").trim().getBytes("US-ASCII")));
            doc.getDocumentElement().normalize();
            String messageType = doc.getDocumentElement().getNodeName();
            if (messageType.equalsIgnoreCase("TOPUPRESPONSE")) {
                // this.messageType = MESSAGE_TYPE; already set in the constructor, left here as information
                // 1. Search for result code
                this.resultCode = MessageTools.getStringValue("RESULT", doc);
                // 2. Search for result text
                this.resultText = MessageTools.getStringValue("RESULTTEXT", doc);
                // 3. Search for terminal id
                this.terminalId = MessageTools.getStringValue("TERMINALID", doc);
                // 4. Search for transaction id
                this.transactionId = MessageTools.getStringValue("TXID", doc);
                // 5. Search for product id
                this.productId = MessageTools.getStringValue("PRODUCTID", doc);
            } else {
                throw new Exception("XML content does not corresponds to a checkrequest message");
            }
        } catch (Exception localException) {
            throw localException;
        }
    }

    public CTopUpResponseMessage() {
        super();
        this.resultCode = null;
        this.resultText = null;
        this.terminalId = null;
        this.transactionId = null;
        this.productId = null;
    }


    public CTopUpResponseMessage(String resultCode, String resultText, String terminalId, String transactionId,
                                 String productId) {
        this();
        this.resultCode = resultCode;
        this.resultText = resultText;
        this.terminalId = terminalId;
        this.transactionId = transactionId;
        this.productId = productId;
    }

}
