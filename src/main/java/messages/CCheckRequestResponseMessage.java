package messages;

import org.w3c.dom.Document;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import java.io.ByteArrayInputStream;

public class CCheckRequestResponseMessage implements IMessage {
    // constants
    public static final String MESSAGE_FORMAT = "<CHECKRESPONSE><RESULT>%s</RESULT><RESULTTEXT>%s</RESULTTEXT><TXID>%s</TXID></CHECKRESPONSE>";


    // attributes
    String resultCode;
    String resultText;
    String transactionId;

    // properties
    public String getResultCode() {
        return resultCode;
    }

    public void setResultCode(String resultCode) {
        this.resultCode = resultCode;
    }

    public String getResultText() {
        return resultText;
    }

    public void setResultText(String resultText) {
        this.resultText = resultText;
    }

    public String getTransactionId() {
        return transactionId;
    }

    public void setTransactionId(String transactionId) {
        this.transactionId = transactionId;
    }

    @Override
    public String getContent() {
        return String.format(MESSAGE_FORMAT, this.resultCode, this.resultText, this.transactionId);
    }

    @Override
    public int getLength() {
        return this.getContent().length();
    }

    @Override
    public void setContent(String xmlContent) throws Exception {
        try {
            if ((xmlContent == null) || ((xmlContent != null) && (xmlContent.trim().equals("")))) {
                throw new IllegalArgumentException("xmlContent empty");
            }

            DocumentBuilderFactory docBuilderFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder docBuilder = docBuilderFactory.newDocumentBuilder();
            Document doc = docBuilder.parse(new ByteArrayInputStream(xmlContent.replaceAll(">\\s*<", "><").trim().getBytes("US-ASCII")));
            doc.getDocumentElement().normalize();
            String messageType = doc.getDocumentElement().getNodeName();
            if (messageType.equalsIgnoreCase("CHECKRESPONSE")) {
                // this.messageType = MESSAGE_TYPE; already set in the constructor, left here as information
                // Search for result code
                this.resultCode = MessageTools.getStringValue("RESULT", doc);
                // Search for result text
                this.resultText = MessageTools.getStringValue("RESULTTEXT", doc);
                // Search for transaction id
                this.transactionId = MessageTools.getStringValue("TXID", doc);
            } else {
                throw new Exception("XML content does not corresponds to a checkrequest message");
            }
        } catch (Exception localException) {
            throw localException;
        }
    }


    public CCheckRequestResponseMessage() {
        super();
        this.resultCode = null;
        this.resultText = null;
        this.transactionId = null;
    }


    public CCheckRequestResponseMessage(String resultCode, String resultText, String transactionId) {
        this();
        this.resultCode = resultCode;
        this.resultText = resultText;
        this.transactionId = transactionId;
    }


}
