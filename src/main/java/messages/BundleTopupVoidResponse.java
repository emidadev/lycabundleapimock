package messages;

import org.w3c.dom.Document;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import java.io.ByteArrayInputStream;

public class BundleTopupVoidResponse implements IMessage {

    public static final String MESSAGE_FORMAT = "<VOIDTOPUPRESPONSE><RESULT>%s</RESULT><RESULTTEXT>%s</RESULTTEXT><TERMINALID>%s</TERMINALID><TXID>%s</TXID><TXREF>%s</TXREF><MSISDN>%s</MSISDN><BUNDLEID>%s</BUNDLEID><BUNDLENAME>%s</BUNDLENAME></VOIDTOPUPRESPONSE>";

    private String resultCode;
    private String resultText;
    private String transactionId;
    private String terminalId;
    private String referenceTransactionId;
    private String msisdn;
    private String bundleId;
    private String bundleName;

    public String getResultCode() {
        return resultCode;
    }

    public void setResultCode(String resultCode) {
        this.resultCode = resultCode;
    }

    public String getResultText() {
        return resultText;
    }

    public void setResultText(String resultText) {
        this.resultText = resultText;
    }

    public String getTransactionId() {
        return transactionId;
    }

    public void setTransactionId(String transactionId) {
        this.transactionId = transactionId;
    }

    public String getTerminalId() {
        return terminalId;
    }

    public void setTerminalId(String terminalId) {
        this.terminalId = terminalId;
    }

    public String getReferenceTransactionId() {
        return referenceTransactionId;
    }

    public void setReferenceTransactionId(String referenceTransactionId) {
        this.referenceTransactionId = referenceTransactionId;
    }

    public String getMsisdn() {
        return msisdn;
    }

    public void setMsisdn(String msisdn) {
        this.msisdn = msisdn;
    }

    public String getBundleId() {
        return bundleId;
    }

    public void setBundleId(String bundleId) {
        this.bundleId = bundleId;
    }

    public String getBundleName() {
        return bundleName;
    }

    public void setBundleName(String bundleName) {
        this.bundleName = bundleName;
    }

    public BundleTopupVoidResponse() {
        this.resultCode = null;
        this.resultText = null;
        this.transactionId = null;
        this.terminalId = null;
        this.referenceTransactionId = null;
        this.msisdn = null;
        this.bundleId = null;
        this.bundleName = null;
    }

    public BundleTopupVoidResponse(String resultCode, String resultText, String transactionId, String terminalId,
                                   String referenceTransactionId, String msisdn, String bundleId, String bundleName) {
        this.resultCode = resultCode;
        this.resultText = resultText;
        this.transactionId = transactionId;
        this.terminalId = terminalId;
        this.referenceTransactionId = referenceTransactionId;
        this.msisdn = msisdn;
        this.bundleId = bundleId;
        this.bundleName = bundleName;
    }

    @Override
    public int getLength() {
        return this.getContent().length();
    }

    @Override
    public String getContent() {
        return String.format(MESSAGE_FORMAT, this.resultCode, this.resultText, this.terminalId, this.transactionId,
                this.referenceTransactionId, this.msisdn, this.bundleId, this.bundleName);
    }

    @Override
    public void setContent(String xmlContent) throws Exception {
        try {
            if ((xmlContent == null) || ((xmlContent != null) && (xmlContent.trim().equals("")))) {
                throw new IllegalArgumentException("xmlContent empty");
            }

            DocumentBuilderFactory docBuilderFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder docBuilder = docBuilderFactory.newDocumentBuilder();
            Document doc = docBuilder.parse(new ByteArrayInputStream(xmlContent.replaceAll(">\\s*<", "><").trim().getBytes("US-ASCII")));
            doc.getDocumentElement().normalize();
            String messageType = doc.getDocumentElement().getNodeName();
            if (messageType.equalsIgnoreCase("VOIDTOPUPRESPONSE")) {
                // this.messageType = MESSAGE_TYPE; already set in the constructor, left here as information
                this.resultCode = MessageTools.getStringValue("RESULT", doc);
                this.resultText = MessageTools.getStringValue("RESULTTEXT", doc);
                this.terminalId = MessageTools.getStringValue("TERMINALID", doc);
                this.transactionId = MessageTools.getStringValue("TXID", doc);
                this.referenceTransactionId = MessageTools.getStringValue("TXREF", doc);
                this.msisdn = MessageTools.getStringValue("MSISDN", doc);
                this.bundleId = MessageTools.getStringValue("BUNDLEID", doc);
                this.bundleName = MessageTools.getStringValue("BUNDLENAME", doc);
            } else {
                throw new Exception("XML content does not corresponds to a checkrequest message");
            }
        } catch (Exception localException) {
            throw localException;
        }
    }
}
