package messages;

import org.w3c.dom.Document;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import java.io.ByteArrayInputStream;

public class CCheckRequestMessage extends CMessageRequest implements IMessage {
    // constants
    public static final String MESSAGE_FORMAT = "<CHECKREQUEST><MESSAGETYPE>%s</MESSAGETYPE><PRODUCTID>%s</PRODUCTID><MSISDN>%s</MSISDN><AMOUNT>%s</AMOUNT><TXID>%s</TXID></CHECKREQUEST>";

    public static final String MESSAGE_TYPE = "CHECKREQUEST";


    // attributes
    String productId;
    String msisdn;
    String amount;
    String transactionId;

    // properties

    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }

    public String getMsisdn() {
        return msisdn;
    }

    public void setMsisdn(String msisdn) {
        this.msisdn = msisdn;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getTransactionId() {
        return transactionId;
    }

    public void setTransactionId(String transactionId) {
        this.transactionId = transactionId;
    }

    @Override
    public void setContent(String xmlContent) throws Exception {
        try {
            if ((xmlContent == null) || ((xmlContent != null) && (xmlContent.trim().equals("")))) {
                throw new IllegalArgumentException("xmlContent empty");
            }
            DocumentBuilderFactory docBuilderFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder docBuilder = docBuilderFactory.newDocumentBuilder();
            Document doc = docBuilder.parse(new ByteArrayInputStream(xmlContent.replaceAll(">\\s*<", "><").trim().getBytes("US-ASCII")));
            doc.getDocumentElement().normalize();
            String messageType = doc.getDocumentElement().getNodeName();
            if (messageType.equalsIgnoreCase("CHECKREQUEST")) // Initial message tag, not the message type
            {
                // this.messageType = MESSAGE_TYPE; already set in the constructor, left here as information
                // Search for product id
                this.productId = MessageTools.getStringValue("PRODUCTID", doc);
                // Search for msisdn
                this.msisdn = MessageTools.getStringValue("MSISDN", doc);
                // Search for amount
                this.amount = MessageTools.getStringValue("AMOUNT", doc);
                // Search for transaction id
                this.transactionId = MessageTools.getStringValue("TXID", doc);
            } else {
                throw new Exception("XML content does not corresponds to a checkrequest message");
            }
        } catch (Exception localException) {
            throw localException;
        }
    }

    @Override
    public String getContent() {
        return String.format(MESSAGE_FORMAT, this.messageType, this.productId, this.msisdn, this.amount, this.transactionId);
    }

    @Override
    public int getLength() {
        return this.getContent().length();
    }


    // methods
    public CCheckRequestMessage() {
        super();
        this.messageType = MESSAGE_TYPE;
        this.productId = null;
        this.msisdn = null;
        this.amount = null;
        this.transactionId = null;
    }


    public CCheckRequestMessage(String productId, String msisdn, String amount, String transactionId) {
        this();
        this.productId = productId;
        this.msisdn = msisdn;
        this.amount = amount;
        this.transactionId = transactionId;
    }

}
