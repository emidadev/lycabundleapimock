/**
 *
 */
package messages;

import org.w3c.dom.Document;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import java.io.ByteArrayInputStream;

/**
 * @author fernandob
 *
 */
public class CQueryRequestResponseBundleMessage implements IMessage {
    // constants
    public static final String MESSAGE_FORMAT = "<QUERYRESPONSE><RESULT>%s</RESULT><RESULTTEXT>%s</RESULTTEXT><TXID>%s</TXID><TXREF>%s</TXREF><BUNDLEID>%s</BUNDLEID></QUERYRESPONSE>";


    // attributes
    String resultCode;
    String resultText;
    String transactionId;
    String referenceTransactionId;
    String bundleId;


    // properties

    public String getResultCode() {
        return resultCode;
    }

    public void setResultCode(String resultCode) {
        this.resultCode = resultCode;
    }

    public String getResultText() {
        return resultText;
    }

    public void setResultText(String resultText) {
        this.resultText = resultText;
    }

    public String getTransactionId() {
        return transactionId;
    }

    public void setTransactionId(String transactionId) {
        this.transactionId = transactionId;
    }

    public String getReferenceTransactionId() {
        return referenceTransactionId;
    }

    public void setReferenceTransactionId(String referenceTransactionId) {
        this.referenceTransactionId = referenceTransactionId;
    }

    public String getBundleId() {
        return bundleId;
    }

    public void setBundleId(String bundleId) {
        this.bundleId = bundleId;
    }

    // methods


    /* (non-Javadoc)
     * @see com.debisys.h2h.providers.lycaMobile.V1010.Messages.IMessage#getContent()
     */
    @Override
    public String getContent() {
        return String.format(MESSAGE_FORMAT, this.resultCode, this.resultText, this.transactionId, this.referenceTransactionId, this.bundleId);
    }

    /* (non-Javadoc)
     * @see com.debisys.h2h.providers.lycaMobile.V1010.Messages.IMessage#getLength()
     */
    @Override
    public int getLength() {
        return this.getContent().length();
    }

    /* (non-Javadoc)
     * @see com.debisys.h2h.providers.lycaMobile.V1010.Messages.IMessage#setContent(java.lang.String)
     */
    @Override
    public void setContent(String xmlContent) throws Exception {
        try {
            if ((xmlContent == null) || ((xmlContent != null) && (xmlContent.trim().equals("")))) {
                throw new IllegalArgumentException("xmlContent empty");
            }

            DocumentBuilderFactory docBuilderFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder docBuilder = docBuilderFactory.newDocumentBuilder();
            Document doc = docBuilder.parse(new ByteArrayInputStream(xmlContent.replaceAll(">\\s*<", "><").trim().getBytes("US-ASCII")));
            doc.getDocumentElement().normalize();
            String messageType = doc.getDocumentElement().getNodeName();
            if (messageType.equalsIgnoreCase("QUERYRESPONSE")) // Initial message tag, not the message type
            {
                // this.messageType = MESSAGE_TYPE; already set in the constructor, left here as information
                // 1. Search for result code
                this.resultCode = MessageTools.getStringValue("RESULT", doc);
                // 2. Search for result text
                this.resultText = MessageTools.getStringValue("RESULTTEXT", doc);
                // 3. Search for transaction id
                this.transactionId = MessageTools.getStringValue("TXID", doc);
                // 4. Search for reference transaction id
                this.referenceTransactionId = MessageTools.getStringValue("TXREF", doc);
                // 5. Search for reference bundle id
                this.bundleId = MessageTools.getStringValue("BUNDLEID", doc);
            } else {
                throw new Exception("XML content does not corresponds to a checkrequest message");
            }
        } catch (Exception localException) {
            throw localException;
        }
    }


    public CQueryRequestResponseBundleMessage() {
        super();
        this.referenceTransactionId = null;
        this.resultCode = null;
        this.resultText = null;
        this.transactionId = null;
        this.bundleId = null;
    }

    public CQueryRequestResponseBundleMessage(String resultCode, String resultText, String transactionId,
                                              String referenceTransactionId, String bundleId) {
        this();
        this.resultCode = resultCode;
        this.resultText = resultText;
        this.transactionId = transactionId;
        this.referenceTransactionId = referenceTransactionId;
        this.bundleId = bundleId;
    }


}
