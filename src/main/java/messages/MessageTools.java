package messages;

import org.w3c.dom.Document;
import org.w3c.dom.NodeList;


public class MessageTools {

    public static String getStringValue(String fieldName, Document document){
        String retValue = "";
        NodeList messageNodes = document.getElementsByTagName(fieldName);
        if((messageNodes != null) && (messageNodes.getLength() > 0))
        {
            retValue = messageNodes.item(0).getTextContent();
        }
        return retValue;
    }
}
