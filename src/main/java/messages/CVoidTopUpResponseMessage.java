/**
 *
 */
package messages;

import org.w3c.dom.Document;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import java.io.ByteArrayInputStream;

/**
 * @author fernandob
 *
 */
public class CVoidTopUpResponseMessage implements IMessage {
    // constants
    public static final String MESSAGE_FORMAT = "<VOIDTOPUPRESPONSE><RESULT>%s</RESULT><RESULTTEXT>%s</RESULTTEXT><TERMINALID>%s</TERMINALID><TXID>%s</TXID><TXREF>%s</TXREF><MSISDN>%s</MSISDN></VOIDTOPUPRESPONSE>";

    // attributes

    String resultCode;
    String resultText;
    String transactionId;
    String terminalId;
    String referenceTransactionId;
    String msisdn;

    // properties

    public String getResultCode() {
        return resultCode;
    }

    public void setResultCode(String resultCode) {
        this.resultCode = resultCode;
    }

    public String getResultText() {
        return resultText;
    }

    public void setResultText(String resultText) {
        this.resultText = resultText;
    }

    public String getTransactionId() {
        return transactionId;
    }

    public void setTransactionId(String transactionId) {
        this.transactionId = transactionId;
    }

    public String getTerminalId() {
        return terminalId;
    }

    public void setTerminalId(String terminalId) {
        this.terminalId = terminalId;
    }

    public String getReferenceTransactionId() {
        return referenceTransactionId;
    }

    public void setReferenceTransactionId(String referenceTransactionId) {
        this.referenceTransactionId = referenceTransactionId;
    }

    public String getMsisdn() {
        return msisdn;
    }

    public void setMsisdn(String msisdn) {
        this.msisdn = msisdn;
    }

    /* (non-Javadoc)
     * @see com.debisys.h2h.providers.lycaMobile.V1010.Messages.IMessage#getContent()
     */
    @Override
    public String getContent() {
        return String.format(MESSAGE_FORMAT, this.resultCode, this.resultText, this.terminalId, this.transactionId, this.referenceTransactionId, this.msisdn);
    }

    /* (non-Javadoc)
     * @see com.debisys.h2h.providers.lycaMobile.V1010.Messages.IMessage#getLength()
     */
    @Override
    public int getLength() {
        return this.getContent().length();
    }

    /* (non-Javadoc)
     * @see com.debisys.h2h.providers.lycaMobile.V1010.Messages.IMessage#setContent(java.lang.String)
     */
    @Override
    public void setContent(String xmlContent) throws Exception {
        try {
            if ((xmlContent == null) || ((xmlContent != null) && (xmlContent.trim().equals("")))) {
                throw new IllegalArgumentException("xmlContent empty");
            }

            DocumentBuilderFactory docBuilderFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder docBuilder = docBuilderFactory.newDocumentBuilder();
            Document doc = docBuilder.parse(new ByteArrayInputStream(xmlContent.replaceAll(">\\s*<", "><").trim().getBytes("US-ASCII")));
            doc.getDocumentElement().normalize();
            String messageType = doc.getDocumentElement().getNodeName();
            if (messageType.equalsIgnoreCase("VOIDTOPUPRESPONSE")) {
                // this.messageType = MESSAGE_TYPE; already set in the constructor, left here as information
                // 1. Search for result code
                this.resultCode = MessageTools.getStringValue("RESULT", doc);
                // 2. Search for result text
                this.resultText = MessageTools.getStringValue("RESULTTEXT", doc);
                // 3. Search for transaction id
                this.transactionId = MessageTools.getStringValue("TXID", doc);
                // 4. Search for terminal id
                this.terminalId = MessageTools.getStringValue("TERMINALID", doc);
                // 5. Search for transaction reference id
                this.referenceTransactionId = MessageTools.getStringValue("TXREF", doc);
                // 6. Search for transaction reference id
                this.msisdn = MessageTools.getStringValue("MSISDN", doc);
            } else {
                throw new Exception("XML content does not corresponds to a checkrequest message");
            }
        } catch (Exception localException) {
            throw localException;
        }
    }


    public CVoidTopUpResponseMessage() {
        super();
        this.resultCode = null;
        this.resultText = null;
        this.transactionId = null;
        this.terminalId = null;
        this.referenceTransactionId = null;
        this.msisdn = null;
    }


    public CVoidTopUpResponseMessage(String resultCode, String resultText, String transactionId, String terminalId,
                                     String referenceTransactionId, String msisdn) {
        this();
        this.resultCode = resultCode;
        this.resultText = resultText;
        this.transactionId = transactionId;
        this.terminalId = terminalId;
        this.referenceTransactionId = referenceTransactionId;
        this.msisdn = msisdn;
    }


}
