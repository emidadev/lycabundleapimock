package messages;

import org.w3c.dom.Document;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import java.io.ByteArrayInputStream;

public class BundleTopupVoidRequest extends CMessageRequest implements IMessage {

    public static final String MESSAGE_FORMAT = "<VOIDTOPUPREQUEST><MESSAGETYPE>%s</MESSAGETYPE><BUNDLEID>%s</BUNDLEID><BUNDLENAME>%s</BUNDLENAME><MSISDN>%s</MSISDN><TERMINALID>%s</TERMINALID><TXID>%s</TXID><CURRENCY>%s</CURRENCY><AMOUNT>%s</AMOUNT><TXREF>%s</TXREF><NO_OF_MONTHS>%s</NO_OF_MONTHS><VAT>%s</VAT></VOIDTOPUPREQUEST>";

    public static final String MESSAGE_TYPE = "BUNDLETOPUP VOID";

    private String referenceTransactionId;
    private String msisdn;
    private String bundleId;
    private String bundleName;
    private String amount;
    private String transactionId;
    private String terminalId;
    private String currency;
    private String vat;
    private String numberOfMonths;

    public String getReferenceTransactionId() {
        return referenceTransactionId;
    }

    public void setReferenceTransactionId(String referenceTransactionId) {
        this.referenceTransactionId = referenceTransactionId;
    }

    public String getMsisdn() {
        return msisdn;
    }

    public void setMsisdn(String msisdn) {
        this.msisdn = msisdn;
    }

    public String getBundleId() {
        return bundleId;
    }

    public void setBundleId(String bundleId) {
        this.bundleId = bundleId;
    }

    public String getBundleName() {
        return bundleName;
    }

    public void setBundleName(String bundleName) {
        this.bundleName = bundleName;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getTransactionId() {
        return transactionId;
    }

    public void setTransactionId(String transactionId) {
        this.transactionId = transactionId;
    }

    public String getTerminalId() {
        return terminalId;
    }

    public void setTerminalId(String terminalId) {
        this.terminalId = terminalId;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public String getVat() {
        return vat;
    }

    public void setVat(String vat) {
        this.vat = vat;
    }

    public String getNumberOfMonths() {
        return numberOfMonths;
    }

    public void setNumberOfMonths(String numberOfMonths) {
        this.numberOfMonths = numberOfMonths;
    }

    public BundleTopupVoidRequest() {
        this.referenceTransactionId = null;
        this.msisdn = null;
        this.bundleId = null;
        this.bundleName = null;
        this.amount = null;
        this.transactionId = null;
        this.terminalId = null;
        this.currency = null;
        this.vat = null;
        this.numberOfMonths = null;
    }

    public BundleTopupVoidRequest(String referenceTransactionId, String msisdn, String bundleId, String bundleName,
                                  String amount, String transactionId, String terminalId, String currency, String vat,
                                  String numberOfMonths) {
        this.referenceTransactionId = referenceTransactionId;
        this.msisdn = msisdn;
        this.bundleId = bundleId;
        this.bundleName = bundleName;
        this.amount = amount;
        this.transactionId = transactionId;
        this.terminalId = terminalId;
        this.currency = currency;
        this.vat = vat;
        this.numberOfMonths = numberOfMonths;
    }

    @Override
    public int getLength() {
        return this.getContent().length();
    }

    @Override
    public String getContent() {
        return String.format(MESSAGE_FORMAT, this.messageType, this.referenceTransactionId, this.msisdn, this.bundleId,
                this.bundleName, this.amount, this.transactionId, this.terminalId, this.currency, this.vat,
                this.numberOfMonths);
    }

    @Override
    public void setContent(String xmlContent) throws Exception {
        try {
            if ((xmlContent == null) || ((xmlContent != null) && (xmlContent.trim().equals("")))) {
                throw new IllegalArgumentException("xmlContent empty");
            }

            DocumentBuilderFactory docBuilderFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder docBuilder = docBuilderFactory.newDocumentBuilder();
            Document doc = docBuilder.parse(new ByteArrayInputStream(xmlContent.replaceAll(">\\s*<", "><").trim().getBytes("US-ASCII")));
            doc.getDocumentElement().normalize();
            String messageType = doc.getDocumentElement().getNodeName();
            if (messageType.equalsIgnoreCase("VOIDTOPUPREQUEST")) // Initial message tag, not the message type
            {
                // this.messageType = MESSAGE_TYPE; already set in the constructor, left here as information
                this.referenceTransactionId = MessageTools.getStringValue("TXREF", doc);
                this.msisdn = MessageTools.getStringValue("MSISDN", doc);
                this.bundleId = MessageTools.getStringValue("BUNDLEID", doc);
                this.bundleName = MessageTools.getStringValue("BUNDLENAME", doc);
                this.amount = MessageTools.getStringValue("AMOUNT", doc);
                this.transactionId = MessageTools.getStringValue("TXID", doc);
                this.terminalId = MessageTools.getStringValue("TERMINALID", doc);
                this.currency = MessageTools.getStringValue("CURRENCY", doc);
                this.vat = MessageTools.getStringValue("VAT", doc);
                this.numberOfMonths = MessageTools.getStringValue("NO_OF_MONTHS", doc);
            } else {
                throw new Exception("XML content does not corresponds to a checkrequest message");
            }
        } catch (Exception localException) {
            throw localException;
        }

    }
}
