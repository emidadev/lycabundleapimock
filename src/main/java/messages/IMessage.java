package messages;


/**
 * This interface represents the common behavior for Lyca messages 
 * @author fernandob
 *
 */
public interface IMessage
{
	public int getLength();
	public String getContent();
	public void setContent(String xmlContent) throws Exception;
}
