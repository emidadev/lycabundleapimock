package responseProcessors;

import configuration.ResponseConfiguration;
import messages.IMessage;
import org.apache.commons.lang3.StringUtils;

public abstract class BaseResponseProcessor implements IResponseProcessor {
    protected ResponseConfiguration responseConfiguration;
    protected IMessage response;

    public IMessage getResponse() {
        return response;
    }

    public BaseResponseProcessor(ResponseConfiguration responseConfiguration) {
        this.responseConfiguration = responseConfiguration;
    }

    @Override
    public ResponseConfiguration getResponseConfiguration() {
        return responseConfiguration;
    }

    // Template Pattern
    @Override
    public String getResponse(String requestXml) throws Exception {
        if (!StringUtils.isEmpty(requestXml)) {
            this.setRequest(requestXml);
            this.setResponse();
            if(this.responseConfiguration.getResponseDelay() > 0) {
                Thread.sleep(this.responseConfiguration.getResponseDelay());
            }
            return this.response.getContent();
        } else {
            throw new IllegalArgumentException("Xml request can't be null or empty");
        }
    }
}
