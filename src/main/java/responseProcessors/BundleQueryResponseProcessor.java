package responseProcessors;

import configuration.ResponseConfiguration;
import messages.CQueryRequestBundleMessage;
import messages.CQueryRequestResponseBundleMessage;

public class BundleQueryResponseProcessor extends BaseResponseProcessor{

    CQueryRequestBundleMessage request;

    public CQueryRequestBundleMessage getRequest() {
        return request;
    }

    public BundleQueryResponseProcessor(ResponseConfiguration responseConfiguration) {
        super(responseConfiguration);
        this.request = null;
        this.response = null;
    }

    @Override
    public void setRequest(String requestXml) throws Exception {
        this.request = new CQueryRequestBundleMessage();
        this.request.setContent(requestXml);
    }

    @Override
    public void setResponse() {
        if(this.request != null){
            CQueryRequestResponseBundleMessage bundleQueryResponse = new CQueryRequestResponseBundleMessage();
            bundleQueryResponse.setResultCode(this.responseConfiguration.getResponseCode());
            bundleQueryResponse.setResultText(this.responseConfiguration.getResponseMessage());
            bundleQueryResponse.setTransactionId(this.request.getTransactionId());
            bundleQueryResponse.setReferenceTransactionId(this.request.getReferenceTransactionId());
            bundleQueryResponse.setBundleId(this.request.getBundleId());
            this.response = bundleQueryResponse;
        } else {
            throw new NullPointerException("Request message is empty");
        }
    }
}
