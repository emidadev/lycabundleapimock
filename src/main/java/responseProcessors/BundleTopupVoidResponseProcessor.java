package responseProcessors;

import configuration.ResponseConfiguration;
import messages.BundleTopupVoidRequest;
import messages.BundleTopupVoidResponse;

public class BundleTopupVoidResponseProcessor extends BaseResponseProcessor {

    private BundleTopupVoidRequest request;

    public BundleTopupVoidRequest getRequest() {
        return request;
    }

    public BundleTopupVoidResponseProcessor(ResponseConfiguration responseConfiguration) {
        super(responseConfiguration);
        this.request = null;
        this.response = null;
    }

    @Override
    public void setRequest(String requestXml) throws Exception {
        this.request = new BundleTopupVoidRequest();
        this.request.setContent(requestXml);
    }

    @Override
    public void setResponse() {
        if (this.request != null) {
            BundleTopupVoidResponse voidResponse = new BundleTopupVoidResponse();
            voidResponse.setResultCode(this.responseConfiguration.getResponseCode());
            voidResponse.setResultText(this.responseConfiguration.getResponseMessage());
            voidResponse.setTransactionId(this.request.getTransactionId());
            voidResponse.setTerminalId(this.request.getTerminalId());
            voidResponse.setReferenceTransactionId(this.request.getReferenceTransactionId());
            voidResponse.setMsisdn(this.request.getMsisdn());
            voidResponse.setBundleId(this.request.getBundleId());
            voidResponse.setBundleName(this.request.getBundleName());
            this.response = voidResponse;
        } else {
            throw new NullPointerException("Request message is empty");
        }
    }
}
