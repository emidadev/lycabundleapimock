package responseProcessors;

import configuration.ResponseConfiguration;
import messages.CTopUpBundleMessage;
import messages.CTopUpResponseBundleMessage;


public class BundleTopupResponseProcessor extends BaseResponseProcessor {

    private CTopUpBundleMessage request;

    public CTopUpBundleMessage getRequest() {
        return request;
    }

    public BundleTopupResponseProcessor(ResponseConfiguration responseConfiguration) {
        super(responseConfiguration);
        this.request = null;
        this.response = null;
    }

    @Override
    public void setRequest(String requestXml) throws Exception {
        this.request = new CTopUpBundleMessage();
        this.request.setContent(requestXml);
    }

    @Override
    public void setResponse() throws NullPointerException {
        if (this.request != null) {
            CTopUpResponseBundleMessage bundleTopupResponse = new CTopUpResponseBundleMessage();
            bundleTopupResponse.setResultCode(this.responseConfiguration.getResponseCode());
            bundleTopupResponse.setResultText(this.responseConfiguration.getResponseMessage());
            bundleTopupResponse.setTransactionId(String.valueOf((int) ((Math.random() * 9000000) + 1000000)));
            bundleTopupResponse.setTerminalId(this.request.getTerminalId());
            bundleTopupResponse.setProductId(this.request.getProductId());
            bundleTopupResponse.setBundleId(this.request.getBundleId());
            bundleTopupResponse.setBundleName(this.request.getBundleName());
            bundleTopupResponse.setMsisdn(this.request.getMsisdn());
            this.response = bundleTopupResponse;
        } else {
            throw new NullPointerException("Request message is empty");
        }
    }
}
