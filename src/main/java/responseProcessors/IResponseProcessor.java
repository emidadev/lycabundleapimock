package responseProcessors;

import configuration.ResponseConfiguration;

public interface IResponseProcessor {
    ResponseConfiguration getResponseConfiguration();

    String getResponse(String requestXml) throws Exception;

    void setRequest(String requestXml) throws Exception;

    void setResponse();
}
