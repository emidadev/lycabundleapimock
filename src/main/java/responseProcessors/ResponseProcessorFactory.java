package responseProcessors;

import configuration.ConfigurationManager;
import configuration.ResponseConfiguration;
import org.apache.commons.lang3.StringUtils;

import java.util.logging.Logger;

public class ResponseProcessorFactory {

    private final static Logger LOGGER = Logger.getLogger(ResponseProcessorFactory.class.getName());

    public static String CHK_BUNDLE_SUBSCRIPTION_MSG_TYPE = "CHECK BUNDLE SUBSCRIPTION";
    public static String BUNDLE_TOPUP_MSG_TYPE = "BUNDLE TOPUP";
    public static String BUNDLE_QUERY_MSG_TYPE = "BUNDLE QUERY";
    public static String BUNDLE_TOPUP_VOID_MSG_TYPE = "BUNDLETOPUP VOID";

    public static IResponseProcessor getResponseProcessor(String requestXml, ConfigurationManager configurationManager){
        IResponseProcessor retValue = null;
        ResponseConfiguration responseConfiguration = null;

        if(!StringUtils.isEmpty(requestXml)){
            if(requestXml.contains(CHK_BUNDLE_SUBSCRIPTION_MSG_TYPE)){
                responseConfiguration = getResponseConfigurationByClassName(CheckBundleSubscriptionResponseProcessor.class.getCanonicalName(), configurationManager);
                CheckBundleSubscriptionResponseProcessor checkBundleResponseProcessor = new CheckBundleSubscriptionResponseProcessor(responseConfiguration);
                retValue = checkBundleResponseProcessor;
            } else if(requestXml.contains(BUNDLE_TOPUP_MSG_TYPE)){
                responseConfiguration = getResponseConfigurationByClassName(BundleTopupResponseProcessor.class.getCanonicalName(), configurationManager);
                BundleTopupResponseProcessor bundleTopupResponseProcessor = new BundleTopupResponseProcessor(responseConfiguration);
                retValue = bundleTopupResponseProcessor;
            } else if(requestXml.contains(BUNDLE_QUERY_MSG_TYPE)){
                responseConfiguration = getResponseConfigurationByClassName(BundleQueryResponseProcessor.class.getCanonicalName(), configurationManager);
                BundleQueryResponseProcessor bundleQueryResponseProcessor = new BundleQueryResponseProcessor(responseConfiguration);
                retValue = bundleQueryResponseProcessor;
            } else if(requestXml.contains(BUNDLE_TOPUP_VOID_MSG_TYPE)){
                responseConfiguration = getResponseConfigurationByClassName(BundleTopupVoidResponseProcessor.class.getCanonicalName(), configurationManager);
                BundleTopupVoidResponseProcessor bundleTopupVoidResponseProcessor = new BundleTopupVoidResponseProcessor(responseConfiguration);
                retValue = bundleTopupVoidResponseProcessor;
            } else {
                LOGGER.severe("No response processor could be created!!!!");
            }
        }

        return retValue;
    }

    private static ResponseConfiguration getResponseConfigurationByClassName(String className, ConfigurationManager configurationManager){
        ResponseConfiguration responseConfiguration = configurationManager.getResponseProcessorConfiguration(className);
        if(responseConfiguration == null){
            responseConfiguration.setResponseProcessorClass(className);
            responseConfiguration = new ResponseConfiguration();
            responseConfiguration.setResponseCode("00");
            responseConfiguration.setResponseMessage("Successful");
            responseConfiguration.setResponseDelay(0);
            LOGGER.warning(String.format("Response Configuration could not be found for class %s. Default response configuration will be used"));
        }
        return responseConfiguration;
    }
}
