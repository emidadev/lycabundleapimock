package responseProcessors;

import configuration.ResponseConfiguration;
import messages.CCheckRequestResponseBundleMessage;
import messages.CheckBundleSubscription;

import java.math.BigDecimal;

public class CheckBundleSubscriptionResponseProcessor extends BaseResponseProcessor {

    private CheckBundleSubscription request;


    public CheckBundleSubscription getRequest() {
        return request;
    }

    public CheckBundleSubscriptionResponseProcessor(ResponseConfiguration responseConfiguration) {
        super(responseConfiguration);
        this.request = null;
        this.response = null;
    }

    @Override
    public void setRequest(String requestXml) throws Exception {
        this.request = new CheckBundleSubscription();
        this.request.setContent(requestXml);
    }

    @Override
    public void setResponse() throws NullPointerException {
        if (this.request != null) {
            CCheckRequestResponseBundleMessage checkBundleSubscriptionResponse = new CCheckRequestResponseBundleMessage();
            checkBundleSubscriptionResponse.setResultCode(this.responseConfiguration.getResponseCode());
            checkBundleSubscriptionResponse.setResultText(this.responseConfiguration.getResponseMessage());
            checkBundleSubscriptionResponse.setTransactionId(String.valueOf((int) ((Math.random() * 9000000) + 1000000)));
            checkBundleSubscriptionResponse.setTerminalId(this.request.getTerminalId());
            checkBundleSubscriptionResponse.setProductId(this.request.getProductId());
            checkBundleSubscriptionResponse.setBundleId(this.request.getBundleId());
            checkBundleSubscriptionResponse.setBundleName(this.request.getBundleName());
            checkBundleSubscriptionResponse.setMsisdn(this.request.getMsisdn());
            checkBundleSubscriptionResponse.setTotalInstallmentsCost(this.getTotalInstallmentsCost());
            this.response = checkBundleSubscriptionResponse;
        } else {
            throw new NullPointerException("Request message is empty");
        }
    }

    private String getTotalInstallmentsCost() {
        BigDecimal requestAmount = (new BigDecimal(this.request.getAmount())).divide(new BigDecimal("100"));
        int requestNumberOfMonths = Integer.parseInt(this.request.getNumberOfMonths());
        return requestAmount.multiply(new BigDecimal(requestNumberOfMonths)).multiply(new BigDecimal("100")).toPlainString();
    }
}
