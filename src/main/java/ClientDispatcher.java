import configuration.ConfigurationManager;
import org.apache.commons.lang3.StringUtils;
import responseProcessors.IResponseProcessor;
import responseProcessors.ResponseProcessorFactory;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.logging.Level;
import java.util.logging.Logger;

public class ClientDispatcher extends Thread {
    private final static Logger LOGGER = Logger.getLogger(ClientDispatcher.class.getName());

    private static final int MESSAGE_HEADER_LENGTH = 4;
    private static final int DEFAULT_BUFFER_SIZE = 400;
    private static final String MESSAGE_FORMAT = "%04d%s";

    private Socket clientSocket;
    private PrintWriter out;
    private BufferedReader in;
    private ConfigurationManager configurationManager;

    public ClientDispatcher(Socket clientSocket, ConfigurationManager configurationManager) {
        this.clientSocket = clientSocket;
        this.configurationManager = configurationManager;
    }

    @Override
    public void run() {

        try {
            out = new PrintWriter(clientSocket.getOutputStream(), true);
            in = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));
            DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
            String requestMessage = this.readMessageContent();
            System.out.println(String.format("%s Request message arrived   : %s", dtf.format(LocalDateTime.now()), requestMessage));
            IResponseProcessor responseProcessor = ResponseProcessorFactory.getResponseProcessor(requestMessage, configurationManager);
            if (responseProcessor != null) {
                try {
                    String responseMessage = responseProcessor.getResponse(requestMessage.substring(4));
                    String returnedMessage = String.format(MESSAGE_FORMAT, responseMessage.length(), responseMessage);
                    System.out.println(String.format("%s Response message returned : %s", dtf.format(LocalDateTime.now()), returnedMessage));
                    out.println(returnedMessage);
                    out.flush();
                } catch (Exception e) {
                    LOGGER.log(Level.SEVERE, "Error while generating response message", e);
                }
            } else {
                LOGGER.log(Level.SEVERE, "No response processor could be found");
            }
        } catch (IOException ioException) {

        } finally {
            if (in != null) {
                try {
                    in.close();
                    if (out != null) out.close();
                    clientSocket.close();
                } catch (IOException e) {
                    LOGGER.log(Level.SEVERE, "Error while closing communication objects", e);
                }
            }
        }
    }


    private String readMessageContent() throws IOException {
        char[] buffer = new char[DEFAULT_BUFFER_SIZE];

        int messageLength = this.readMessageLength();

        this.in.read(buffer, 0, messageLength);

        return String.format(MESSAGE_FORMAT, messageLength, (new String(buffer)).trim());
    }

    private int readMessageLength() {
        int retValue = 0;
        String strLength = "";
        for (int i = 0; i < MESSAGE_HEADER_LENGTH; i++) {
            try {
                strLength += (char) in.read();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        if (StringUtils.isNumeric(strLength)) {
            retValue = Integer.parseInt(strLength);
        }
        return retValue;
    }
}
