import configuration.ConfigurationManager;
import org.apache.commons.lang3.StringUtils;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.logging.Level;
import java.util.logging.Logger;


public class LycaBundleApiMock {
    private static final Logger LOGGER = Logger.getLogger(LycaBundleApiMock.class.getName());

    private static final int DEFAULT_SERVER_PORT = 10300;


    public static void main(String[] args) {
        LycaServer server = new LycaServer();

        try {
            if(areProgramArgumentsValid(args)){
                ConfigurationManager configurationManager = new ConfigurationManager(args[1]);
                server.start(getServerPortFromArguments(args), configurationManager);
            }
        } catch (IOException e) {
            LOGGER.log(Level.SEVERE, "General error on server operation", e);
        }
    }

    private static int getServerPortFromArguments(String[] args) {
        int serverPort = DEFAULT_SERVER_PORT;

        if ((args != null) && (args.length > 0)) {
            try {
                serverPort = Integer.parseInt(args[0]);
            } catch (NumberFormatException numberFormatException) {
                LOGGER.warning(String.format("Default server port could not be read from program arguments. Using default port : %d", DEFAULT_SERVER_PORT));
            }
        }

        return serverPort;
    }

    private static boolean areProgramArgumentsValid(String[] args) {
        if ((args == null) || (args.length != 2)) {
            LOGGER.info("Program parameters not supplied or partially supplied.");
            LOGGER.info("Program run : java -jar LycaBundleApiMock.jar [PORT] [Configuration File Path]");
            return false;
        }

        if ((!StringUtils.isEmpty(args[0])) && (!StringUtils.isNumeric(args[0]))) {
            LOGGER.info("The supplied port is invalid");
            return false;
        }

        if ((!StringUtils.isEmpty(args[1])) && (!Files.exists(Paths.get(args[1])))) {
            LOGGER.info("The supplied file configuration path is invalid");
            return false;
        }
        return true;
    }

}
